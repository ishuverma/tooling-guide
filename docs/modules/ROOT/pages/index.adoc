= Portfolio Architecture Tooling Workshop
:navtitle: Overview


This workshop will teach you how to use draw.io tooling to create architectural diagrams
with Red Hat Portfolio Architecture (PA) design elements.

You can also use this tooling to edit the existing PA diagrams available at link:https://gitlab.com/osspa/portfolio-architecture-examples[Portfolio Architecture Example Repository]
